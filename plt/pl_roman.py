import numpy as np

# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)

import matplotlib.pyplot as plt

cmap = plt.cm.get_cmap('viridis')


yellow = "#ffa500"
blue = "#1e76b3"

colors = ['#FFB200', '#404040', '#5CE06F', '#3E78B2', '#FF724F']
colors = ['#FFB200', '#404040', '#7A5C61', '#5B9279', '#33658A']




fig = plt.figure(num=None, figsize=(6, 3.5), dpi=100, facecolor='w', edgecolor='k')

data = np.loadtxt('data/pl_roman/04.txt')
data = data[data[:,1]>0]
data[:,0] = 1.2398/data[:,0]*1e3
plt.semilogy(data[:,0], data[:,1]*1e2, label="bulk ZnO", color=yellow)

# data = np.loadtxt('data/pl_roman/01.txt')
# data = data[data[:,1]>0]
# plt.semilogy(data[:,0], data[:,1], label="ZnO nanocrystals (ann. at 300 °C)", color=colors[0])

data = np.loadtxt('data/pl_roman/02a.txt')
data = data[data[:,1]>0]
data[:,0] = 1.2398/data[:,0]*1e3
plt.semilogy(data[:,0], data[:,1], label="ZnO nanocrystals", color=cmap(0.75))

data = np.loadtxt('data/pl_roman/02b.txt')
data = data[data[:,1]>0]
data[:,0] = 1.2398/data[:,0]*1e3
plt.semilogy(data[:,0], data[:,1]*0.55e2, color=cmap(0.75))

data = np.loadtxt('data/pl_roman/03a.txt')
data = data[data[:,1]>0]
data[:,0] = 1.2398/data[:,0]*1e3
plt.semilogy(data[:,0], data[:,1]*0.2e1, label="ZnO nanorods", color=blue)

ax = plt.gca()
ax.get_yaxis().set_ticks([])

plt.xlim([350,850])
plt.xlabel('Wavelength (nm)')
plt.ylabel('Intensity (a.u.)')
plt.legend(loc='best')

plt.tight_layout()
#plt.show()
plt.savefig('out/pl_roman.pdf')
plt.savefig('out/pl_roman.pgf')
