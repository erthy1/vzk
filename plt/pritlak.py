# -*- coding: utf-8 -*-

import numpy as np
from io import StringIO   # StringIO behaves like a file object
import glob
import os

import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)

import matplotlib.pyplot as plt


def get_separate_measurements(datafile):
    """
    Takes a file, separates measurements and returns a list of faked files.
    """
    with open(datafile, 'r', encoding='utf-8') as myfile:
        data = myfile.read()
        data = data.split('\n\n')
        sweep_id = data[0].split('\n')[0].replace("=", "").replace("#  Sweep ID: ", "")
    datafiles = []
    for datastring in data:
        datafiles.append(StringIO(datastring))

    return datafiles, sweep_id


def get_datafiles(directory):
    os.chdir(directory)
    files = []
    for file in glob.glob("*.txt"):
        files.append(file)
    return files[0:25]


import os
# datafiles = ['20170110_GaN\sweep_2017-01-10_10.32.31.467937_0_0.txt'] * 4
# datafiles = get_datafiles('20170110_GaN_2\grid_input')
datafiles = ['data/pritlak/data.txt']

fig = plt.figure(num=1, figsize=(6, 3.5), dpi=100, facecolor='w', edgecolor='k')
plt.clf()

dfiles, sweep_id = get_separate_measurements(datafiles[0])

# 6V_4V_3V_2V_1V_0V_-1V
flist = [dfiles[0:5], dfiles[6:10], dfiles[11:15], dfiles[17:22],
         dfiles[23:25], dfiles[27:33], dfiles[34:40]]
labels = ["6 V", "4 V", "3 V", "2 V", "1 V", "0 V", "-1 V"]
#colors = [blue, green, orange, cyan, yellow]
for i in range(len(flist)):
    to_avg = []
    for dfile in flist[i]:
        x, y = np.loadtxt(dfile, comments='#', unpack=True)
        plt.plot(x*1e6, y, color="gray", alpha=0.3, lw=0.5)
        to_avg.append(y)

    np_to_avg = np.array(to_avg)
    y_avg = np.average(np_to_avg, axis=0, weights=None)
    plt.plot(x*1e6, y_avg, label=labels[i], lw=1.5)

# plt.ylim((0.7, 10.5))
# plt.title(title)

plt.xlim((-0.000010*1e6, 0.000010*1e6))

plt.xlabel('I (nA)')
plt.ylabel('U (V)')

plt.legend(loc=2)

#plt.show()

plt.tight_layout()

plt.savefig('out/pritlak' + '.pdf', dpi=300)
plt.savefig('out/pritlak' + '.pgf', dpi=300)
